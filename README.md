# README #

This is the simple monitoring script which includes the following features:

* Check CPU Load
* Check Memory Usage
* Check Disk Space Usage
* Check Network Load (Not sure that it works propertly)
* Send email message

### How do I get set up? ###

* You need to change some settings in the script in the **Setup** section:

```
#!bash

# CPU LA critical
CPU_USG_CRIT=0.25

# Memory usage critical
MEM_USG_CRIT=6000

# Disk device or devices separated by space
DSK_SRC="/dev/sda5 /dev/sda8"

# Disk usage critical in percentage. Also used to check inodes usage
DSK_USG_CRIT=90

# Network device or devices separated by space
NET_SRC="br0 eth0"

# Network bandwith critical in kb/s
NET_USG_CRIT=100

```

* Run the script to check if it works

```
#!bash

./simple-monitoring-script.sh email@address.com
```

* Add the script to crontab

```
#!bash
# Example: run script every 5 minutes
*/5 * * * * /path/to/simple-monitoring-script.sh email@address.com 2>/dev/null
```

### Who do I talk to? ###

* Repo owner or admin