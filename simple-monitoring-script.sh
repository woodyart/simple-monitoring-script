#!/bin/bash

#################################
# Setup your parameters:	#
#################################
# CPU LA critical
CPU_USG_CRIT=0.25
# Memory usage critical
MEM_USG_CRIT=6000
# Disk device or devices separated by space
DSK_SRC="/"
# Disk usage critical in percentage. Also used to check inodes usage
DSK_USG_CRIT=4
# Network device or devices separated by space
NET_SRC="eth0"
# Network bandwith critical in kb/s
NET_USG_CRIT=2
# E-mail address assignment
EMAIL_TO=$1
# E-mail subject string
TEMPSTAT="HOST: $(hostname) - WARNING!"
#################################
# /End Setup			#
#################################
check_args(){
	if [ -z "$EMAIL_TO" ]; then
    		echo "No arguments supplied. You must run: $0 email@address.com"
		exit 2
	fi
}

cpu_usage(){
	CPU_USG_CUR=$(cat /proc/loadavg | awk '{print $2}')
	if [[ 1 -eq $(echo "${CPU_USG_CRIT} < ${CPU_USG_CUR}" | bc) ]]; then
		TEMPSTAT="$TEMPSTAT\nWARNING! CPU Load Average: $CPU_USG_CUR"
	fi
}

mem_usage(){
	MEM_TOTAL=$(cat /proc/meminfo | awk /MemTotal/'{ {$2/=1024;} print int($2)}')
	MEM_FREE_CUR=$(cat /proc/meminfo | awk /MemFree/'{ {$2/=1024;} print int($2)}')
	let MEM_USG_CUR=$MEM_TOTAL-$MEM_FREE_CUR
	if [[ 1 -eq $(echo "${MEM_USG_CRIT} < ${MEM_USG_CUR}" | bc) ]]; then
		TEMPSTAT="$TEMPSTAT\nWARNING! Running out of FREE Memory: $MEM_USG_CUR Mb used of $MEM_TOTAL Mb total"
	fi
}

disk_usage(){
	for DSK in $DSK_SRC; do
		# Check space
		DSK_USG_CUR=$(df $DSK | tail -1 | awk '{gsub("%",""); print $5}')
		if [[ 1 -eq $(echo "${DSK_USG_CRIT} < ${DSK_USG_CUR}" | bc) ]]; then
			DSK_FREE_CUR=$(df $DSK -h | tail -1 | awk '{print $4}')
			TEMPSTAT="$TEMPSTAT\nWARNING! Running out of FREE Disk space on $DSK: $DSK_USG_CUR% used"
		fi
		# Check inodes
		DSK_I_USG_CUR=$(df $DSK -i | tail -1 | awk '{gsub("%",""); print $5}')
		if [[ 1 -eq $(echo "${DSK_USG_CRIT} < ${DSK_I_USG_CUR}" | bc) ]]; then
			DSK_I_FREE_CUR=$(df $DSK -i | tail -1 | awk '{print $4}')
			TEMPSTAT="$TEMPSTAT\nWARNING! Running out of FREE Disk Inodes on $DSK: $DSK_I_USG_CUR% used"
		fi
	done
}

net_usage(){
	SLEEP_TIME="1" # sleep time = 1 second
	for NET in $NET_SRC; do
		RX1=$(cat /sys/class/net/$NET/statistics/rx_bytes)
        	TX1=$(cat /sys/class/net/$NET/statistics/tx_bytes)
        	sleep $SLEEP_TIME
        	RX2=$(cat /sys/class/net/$NET/statistics/rx_bytes)
        	TX2=$(cat /sys/class/net/$NET/statistics/tx_bytes)
		RX_KBPS=$(echo "(${RX2} - ${RX1}) / 1024"|bc)
		TX_KBPS=$(echo "(${TX2} - ${TX1}) / 1024"|bc)
		if [[ 1 -eq $(echo "${NET_USG_CRIT} < ${RX_KBPS}" | bc) ]]; then
			TEMPSTAT="$TEMPSTAT\nWARNING! RX speed on $NET: $RX_KBPS KB/s"
		fi
		if [[ 1 -eq $(echo "${NET_USG_CRIT} < ${TX_KBPS}" | bc) ]]; then
			TEMPSTAT="$TEMPSTAT\nWARNING! TX speed on $NET: $TX_KBPS KB/s"
		fi
	done
}

send_email(){
	if [ $( echo -e "$TEMPSTAT" | wc -l ) -gt 1 ]; then
		SUBJECT=$(echo -e "$TEMPSTAT" | sed 'q')
		BODY="$(echo -e "$TEMPSTAT" | sed '1d')"
		echo -e "$BODY" | mail -s "$SUBJECT" $EMAIL_TO
	fi
}
#################################
# Begin				#
#################################
check_args
cpu_usage
mem_usage
disk_usage
net_usage
send_email
